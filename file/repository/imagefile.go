package postgre

import (
	"context"
	"database/sql"

	"gitlab.com/alfathaulia/mt_hippam_api/domain"
)

type postgreeFileRepo struct {
	db *sql.DB
}

func NewPostFileRepo(DB *sql.DB) domain.ImageFileRepository {
	return &postgreeFileRepo{db: DB}
}

func (m *postgreeFileRepo) Fetch(ctx context.Context, cursor string, num int64) (res []domain.ImageFile, nextCursor string, err error)
func (m *postgreeFileRepo) GetByID(ctx context.Context, id int64) (res domain.ImageFile, err error)
func (m *postgreeFileRepo) GetByFilename(ctx context.Context, filename string) (res domain.ImageFile, err error)
func (m *postgreeFileRepo) Update(ctx context.Context, updateData *domain.ImageFile) (err error)
func (m *postgreeFileRepo) Store(ctx context.Context, data *domain.ImageFile) (err error)
func (m *postgreeFileRepo) Delete(ctx context.Context, id int64) (err error)
