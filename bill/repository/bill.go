package postgre

import (
	"context"
	"database/sql"

	"gitlab.com/alfathaulia/mt_hippam_api/domain"
)

type postgreBillRepo struct {
	DB *sql.DB
}

func NewPostgreBillRepo(DB *sql.DB) domain.BillRepository {
	return &postgreBillRepo{DB: DB}
}

func (m *postgreBillRepo) Fetch(ctx context.Context, cursor string, num int64) (res []domain.Bill, nextCursor string, err error)
func (m *postgreBillRepo) GetByID(ctx context.Context, id int64) (res domain.Bill, err error)
func (m *postgreBillRepo) GetByCustomerID(ctx context.Context, customer_id string) (res domain.Bill, err error)
func (m *postgreBillRepo) Update(ctx context.Context, updateData *domain.Bill) (err error)
func (m *postgreBillRepo) Store(ctx context.Context, data *domain.Bill) (err error)
func (m *postgreBillRepo) Delete(ctx context.Context, id int64) (err error)
