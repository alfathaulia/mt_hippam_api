package domain

import (
	"context"
	"time"
)

// Unit ...
type Unit struct {
	ID        int64     `json:"id"`
	UnitName  string    `json:"unit_name" validate:"required"`
	UnitCode  string    `json:"unit_code" validate:"required"`
	ImageUnit string    `json:"image_unit" `
	Alamat    string    `json:"alamat" validate:"required"`
	BiayaAir  int64     `json:"biaya_air" validate:"required"`
	UpdatedAt time.Time `json:"updated_at"`
	CreatedAt time.Time `json:"created_at"`
}

// UnitUsecase represent the Unit's usecases
type UnitUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]Unit, string, error)
	GetByID(ctx context.Context, id int64) (Unit, error)
	Update(ctx context.Context, ar *Unit) error
	GetByUnitName(ctx context.Context, unit_name string) (Unit, error)
	Store(context.Context, *Unit) error
	Delete(ctx context.Context, id int64) error
}

// UnitRepository represent the Unit's repository contract
type UnitRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []Unit, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (Unit, error)
	GetByUnitName(ctx context.Context, unit_name string) (Unit, error)
	Update(ctx context.Context, ar *Unit) error
	Store(ctx context.Context, a *Unit) error
	Delete(ctx context.Context, id int64) error
}
