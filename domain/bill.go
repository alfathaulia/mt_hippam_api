package domain

import (
	"context"
	"time"
)

type BillState string

const (
	BillStateValid      BillState = "valid"
	BillStateTidakValid BillState = "tidak_valid"
	BillStateUncheck    BillState = "uncheck"
	BillStateChecked    BillState = "checked"
)

type BillStatus string

const (
	BillStatusBelumBayar BillStatus = "belum_bayar"
	BillStatusLunas      BillStatus = "lunas"
	BillStatusMenunggu   BillStatus = "menunggu"
)

// Bill ...
type Bill struct {
	ID              int64      `json:"id"`
	CustomerID      int64      `json:"customer_id"`
	Usage           int        `json:"usage"`
	UsageLM         int        `json:"usage_lm" `
	UsageTM         int        `json:"usage_tm"`
	Total           int64      `json:"total"`
	ReferenceNumber string     `json:"reference_num"`
	Status          BillStatus `json:"status"`
	TglBayar        time.Time  `json:"tgl_bayar"`
	Staff           string     `json:"staff"`
	ImageMeter      string     `json:"image_meter"`
	BillState       BillState  `json:"biil_state"`
	UpdatedAt       time.Time  `json:"updated_at"`
	CreatedAt       time.Time  `json:"created_at"`
}

// BillUsecase represent the Bill's usecases
type BillUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]Bill, string, error)
	GetByID(ctx context.Context, id int64) (Bill, error)
	Update(ctx context.Context, ar *Bill) error
	GetByCustomerID(ctx context.Context, customer_id string) (Bill, error)
	Store(context.Context, *Bill) error
	Delete(ctx context.Context, id int64) error
}

// BillRepository represent the Bill's repository contract
type BillRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []Bill, nextCursor string, err error)
	GetByID(ctx context.Context, od int64) (Bill, error)
	GetByCustomerID(ctx context.Context, customer_id string) (Bill, error)
	Update(ctx context.Context, ar *Bill) error
	Store(ctx context.Context, a *Bill) error
	Delete(ctx context.Context, id int64) error
}
