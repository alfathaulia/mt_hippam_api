package domain

import (
	"context"
	"time"
)

// UserAccount ...
type UserAccount struct {
	ID           int64     `json:"id"`
	CustomerID   int64     `json:"customer_id"`
	UserID       User      `json:"user_id"`
	Alamat       string    `json:"alamat" `
	PhotoProfile string    `json:"photo_profile" validate:"required"`
	UpdatedAt    time.Time `json:"updated_at"`
	CreatedAt    time.Time `json:"created_at"`
}

// UserAccountUsecase represent the UserAccount's usecases
type UserAccountUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]UserAccount, string, error)
	GetByID(ctx context.Context, id int64) (UserAccount, error)
	Update(ctx context.Context, ar *UserAccount) error
	GetByCustomerID(ctx context.Context, customer_id int64) (UserAccount, error)
	Store(context.Context, *UserAccount) error
	Delete(ctx context.Context, id int64) error
}

// UserAccountRepository represent the UserAccount's repository contract
type UserAccountRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []UserAccount, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (UserAccount, error)
	GetByCustomerID(ctx context.Context, customer_id int64) (UserAccount, error)
	Update(ctx context.Context, ar *UserAccount) error
	Store(ctx context.Context, a *UserAccount) error
	Delete(ctx context.Context, id int64) error
}
