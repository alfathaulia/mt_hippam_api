package domain

import (
	"context"
	"time"
)

// ImageFile ...
type ImageFile struct {
	ID        int64     `json:"id"`
	Filename  string    `json:"filename"`
	Ext       string    `json:"ext"`
	Path      string    `json:"path"`
	Size      int64     `json:"size"`
	MimeType  string    `json:"mime_type"`
	Url       string    `json:"url"`
	CreatedAt time.Time `json:"created_at"`
}

// ImageFileUsecase represent the ImageFile's usecases
type ImageFileUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]ImageFile, string, error)
	GetByID(ctx context.Context, id int64) (ImageFile, error)
	Update(ctx context.Context, ar *ImageFile) error
	GetByFilename(ctx context.Context, filename string) (ImageFile, error)
	Store(context.Context, *ImageFile) error
	Delete(ctx context.Context, id int64) error
}

// ImageFileRepository represent the ImageFile's repository contract
type ImageFileRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []ImageFile, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (ImageFile, error)
	GetByFilename(ctx context.Context, filename string) (ImageFile, error)
	Update(ctx context.Context, ar *ImageFile) error
	Store(ctx context.Context, a *ImageFile) error
	Delete(ctx context.Context, id int64) error
}
