package domain

import (
	"context"
	"time"
)

// UserAdmin ...
type UserAdmin struct {
	ID          int64     `json:"id"`
	UserID      User      `json:"user_id"`
	NoKTP       string    `json:"no_ktp"`
	NamaKTP     string    `json:"nama_ktp" `
	NoTelp      string    `json:"no_telp"`
	Alamat      string    `json:"alamat"`
	Kecamatan   string    `json:"kecamatan"`
	Kodepos     string    `json:"kodepos"`
	Kelurahan   string    `json:"kelurahan"`
	ImageKTP    string    `json:"image_ktp"`
	IsCompleted bool      `json:"is_completed"`
	UpdatedAt   time.Time `json:"updated_at"`
	CreatedAt   time.Time `json:"created_at"`
}

// UserAdminUsecase represent the UserAdmin's usecases
type UserAdminUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]UserAdmin, string, error)
	GetByID(ctx context.Context, id int64) (UserAdmin, error)
	Update(ctx context.Context, ar *UserAdmin) error
	GetByNoKTP(ctx context.Context, no_ktp string) (UserAdmin, error)
	Store(context.Context, *UserAdmin) error
	Delete(ctx context.Context, id int64) error
}

// UserAdminRepository represent the UserAdmin's repository contract
type UserAdminRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []UserAdmin, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (UserAdmin, error)
	GetByNoKTP(ctx context.Context, no_ktp string) (UserAdmin, error)
	Update(ctx context.Context, ar *UserAdmin) error
	Store(ctx context.Context, a *UserAdmin) error
	Delete(ctx context.Context, id int64) error
}
