package postgre

import (
	"context"
	"database/sql"

	"gitlab.com/alfathaulia/mt_hippam_api/domain"
)

type postgreUnitRepo struct {
	DB *sql.DB
}

func NewPostgreUnitRepo(DB *sql.DB) domain.UnitRepository {
	return &postgreUnitRepo{DB: DB}
}

func (m *postgreUnitRepo) Fetch(ctx context.Context, cursor string, num int64) (res []domain.Unit, nextCursor string, err error)
func (m *postgreUnitRepo) GetByID(ctx context.Context, id int64) (res domain.Unit, err error)
func (m *postgreUnitRepo) GetByUnitName(ctx context.Context, unit_name string) (res domain.Unit, err error)
func (m *postgreUnitRepo) Update(ctx context.Context, updateData *domain.Unit) (err error)
func (m *postgreUnitRepo) Store(ctx context.Context, data *domain.Unit) (err error)
func (m *postgreUnitRepo) Delete(ctx context.Context, id int64) (err error)
