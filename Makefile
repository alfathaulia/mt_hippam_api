test:
	go test -v -cover ./...

mock:
	cd domain && mockery --all --keeptree 

.PHONY: test mock