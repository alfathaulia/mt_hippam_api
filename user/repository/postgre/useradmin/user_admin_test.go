package postgre_useradmin_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/user/repository"
	userAdminPostgreRepo "gitlab.com/alfathaulia/mt_hippam_api/user/repository/postgre/useradmin"
	"gopkg.in/DATA-DOG/go-sqlmock.v2"
)

func TestFetch(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockUserAdmin := []domain.UserAdmin{
		{
			ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "1", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "12", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
		{
			ID: 2, UserID: domain.User{ID: 2}, NoKTP: "1", NamaKTP: "2", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "13", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}
	rows := sqlmock.NewRows([]string{"id", "user_id", "no_ktp", "nama_ktp", "no_telp", "alamat", "kelurahan", "kecamatan", "kodepos", "image_ktp", "is_completed", "updated_at", "created_at"}).
		AddRow(mockUserAdmin[0].ID, mockUserAdmin[0].UserID.ID, mockUserAdmin[0].NoKTP, mockUserAdmin[0].NamaKTP, mockUserAdmin[0].NoTelp, mockUserAdmin[0].Alamat, mockUserAdmin[0].Kelurahan, mockUserAdmin[0].Kecamatan, mockUserAdmin[0].Kodepos, mockUserAdmin[0].ImageKTP, mockUserAdmin[0].IsCompleted, mockUserAdmin[0].UpdatedAt, mockUserAdmin[0].CreatedAt).
		AddRow(mockUserAdmin[1].ID, mockUserAdmin[1].UserID.ID, mockUserAdmin[1].NoKTP, mockUserAdmin[1].NamaKTP, mockUserAdmin[1].NoTelp, mockUserAdmin[1].Alamat, mockUserAdmin[1].Kelurahan, mockUserAdmin[1].Kecamatan, mockUserAdmin[1].Kodepos, mockUserAdmin[1].ImageKTP, mockUserAdmin[1].IsCompleted, mockUserAdmin[1].UpdatedAt, mockUserAdmin[1].CreatedAt)

	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin  ORDER BY created_at LIMIT ?`

	mock.ExpectQuery(query).WillReturnRows(rows)

	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)
	cursor := repository.EncodeCursor(mockUserAdmin[1].CreatedAt)
	num := int64(2)
	list, nextCursor, err := a.Fetch(context.TODO(), cursor, num)
	assert.NoError(t, err)
	assert.NotEmpty(t, nextCursor)
	assert.Len(t, list, 2)
}

func TestGetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	mockUserAdmin := []domain.UserAdmin{
		{
			ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "1", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "12", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "no_ktp", "nama_ktp", "no_telp", "alamat", "kelurahan", "kecamatan", "kodepos", "image_ktp", "is_completed", "updated_at", "created_at"}).
		AddRow(mockUserAdmin[0].ID, mockUserAdmin[0].UserID.ID, mockUserAdmin[0].NoKTP, mockUserAdmin[0].NamaKTP, mockUserAdmin[0].NoTelp, mockUserAdmin[0].Alamat, mockUserAdmin[0].Kelurahan, mockUserAdmin[0].Kecamatan, mockUserAdmin[0].Kodepos, mockUserAdmin[0].ImageKTP, mockUserAdmin[0].IsCompleted, mockUserAdmin[0].UpdatedAt, mockUserAdmin[0].CreatedAt)

	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin  WHERE ID = ?`

	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)

	num := int64(5)
	anArticle, err := a.GetByID(context.TODO(), num)
	assert.NoError(t, err)
	assert.NotNil(t, anArticle)
}

func TestGetByCustomerID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	mockUserAdmin := []domain.UserAdmin{
		{
			ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "1", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "12", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "no_ktp", "nama_ktp", "no_telp", "alamat", "kelurahan", "kecamatan", "kodepos", "image_ktp", "is_completed", "updated_at", "created_at"}).
		AddRow(mockUserAdmin[0].ID, mockUserAdmin[0].UserID.ID, mockUserAdmin[0].NoKTP, mockUserAdmin[0].NamaKTP, mockUserAdmin[0].NoTelp, mockUserAdmin[0].Alamat, mockUserAdmin[0].Kelurahan, mockUserAdmin[0].Kecamatan, mockUserAdmin[0].Kodepos, mockUserAdmin[0].ImageKTP, mockUserAdmin[0].IsCompleted, mockUserAdmin[0].UpdatedAt, mockUserAdmin[0].CreatedAt)

	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin  WHERE no_ktp = ?`
	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)
	NoKTP := "1"
	user, err := a.GetByNoKTP(context.TODO(), NoKTP)
	assert.NoError(t, err)
	assert.NotNil(t, user)
}

func TestStore(t *testing.T) {

	mockUserAdmin := []domain.UserAdmin{
		{
			ID: 1,
			UserID: domain.User{
				ID: 1, Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
			},
			NoKTP:   "1",
			NamaKTP: "1", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "12", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "INSERT  useradmin SET user_id=\\?, no_ktp=\\?, nama_ktp=\\?, no_telp=\\?, alamat=\\?, kelurahan=\\?, kecamatan=\\?, kodepos=\\?, image_ktp=\\?, is_completed=\\?, updated_at=\\?, created_at=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(mockUserAdmin[0].UserID.ID, mockUserAdmin[0].NoKTP, mockUserAdmin[0].NamaKTP, mockUserAdmin[0].NoTelp, mockUserAdmin[0].Alamat, mockUserAdmin[0].Kelurahan, mockUserAdmin[0].Kecamatan, mockUserAdmin[0].Kodepos, mockUserAdmin[0].ImageKTP, mockUserAdmin[0].IsCompleted, mockUserAdmin[0].UpdatedAt, mockUserAdmin[0].CreatedAt).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)

	err = a.Store(context.TODO(), &mockUserAdmin[0])
	assert.NoError(t, err)
	assert.Equal(t, int64(12), mockUserAdmin[0].ID)
}

func TestUpdate(t *testing.T) {

	userAdmin := &domain.UserAdmin{
		ID: 1,
		UserID: domain.User{
			ID: 1, Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
		NoKTP:   "1",
		NamaKTP: "1", NoTelp: "234", Alamat: "sasap", Kelurahan: "fasdfasdf", Kecamatan: "fasdfasdf", Kodepos: "12", ImageKTP: "asdfdas", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	query := "UPDATE useradmin SET user_id=\\?, no_ktp=\\?, nama_ktp=\\?, no_telp=\\?, alamat=\\?, kelurahan=\\?, kecamatan=\\?, kodepos=\\?, image_ktp=\\?, is_completed=\\?, updated_at=\\?  WHERE id=\\?"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(userAdmin.UserID.ID, userAdmin.NoKTP, userAdmin.NamaKTP, userAdmin.NoTelp, userAdmin.Alamat, userAdmin.Kelurahan, userAdmin.Kecamatan, userAdmin.Kodepos, userAdmin.ImageKTP, userAdmin.IsCompleted, userAdmin.UpdatedAt, userAdmin.ID).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)

	err = a.Update(context.TODO(), userAdmin)
	assert.NoError(t, err)
}

func TestDelete(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "DELETE FROM useradmin WHERE id = \\?"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(12).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAdminPostgreRepo.NewPostgreUserAdminAccountRepo(db)

	id := int64(12)
	err = a.Delete(context.TODO(), id)
	assert.NoError(t, err)
}
