package postgre_useradmin

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/user/repository"
)

type postgreUserAdminRepo struct {
	DB *sql.DB
}

// NewPostgreUserAdminAccountRepo will create an object that represent the domain.UserAdminAccountRepository interface
func NewPostgreUserAdminAccountRepo(DB *sql.DB) domain.UserAdminRepository {
	return &postgreUserAdminRepo{DB: DB}
}

func (m *postgreUserAdminRepo) fetch(ctx context.Context, query string, args ...interface{}) (result []domain.UserAdmin, err error) {
	rows, err := m.DB.QueryContext(ctx, query, args...)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			logrus.Error(errRow)
		}
	}()

	result = make([]domain.UserAdmin, 0)
	userID := int64(0)
	for rows.Next() {
		t := domain.UserAdmin{}
		err = rows.Scan(
			&t.ID,
			&userID,
			&t.NoKTP,
			&t.NamaKTP,
			&t.NoTelp,
			&t.Alamat,
			&t.Kelurahan,
			&t.Kodepos,
			&t.Kecamatan,
			&t.ImageKTP,
			&t.IsCompleted,
			&t.UpdatedAt,
			&t.CreatedAt,
		)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		t.UserID = domain.User{
			ID: userID,
		}
		result = append(result, t)
	}
	return result, nil
}

func (m *postgreUserAdminRepo) Fetch(ctx context.Context, cursor string, num int64) (res []domain.UserAdmin, nextCursor string, err error) {
	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin ORDER BY created_at LIMIT ?`

	decodeCursor, err := repository.DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", domain.ErrBadParamInput
	}
	res, err = m.fetch(ctx, query, decodeCursor, num)
	if err != nil {
		return nil, "", err
	}
	if len(res) == int(num) {
		nextCursor = repository.EncodeCursor(res[len(res)-1].CreatedAt)
	}
	return
}
func (m *postgreUserAdminRepo) GetByID(ctx context.Context, id int64) (res domain.UserAdmin, err error) {
	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin WHERE ID = ?`

	list, err := m.fetch(ctx, query, id)
	if err != nil {
		return domain.UserAdmin{}, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}

	return
}
func (m *postgreUserAdminRepo) GetByNoKTP(ctx context.Context, no_ktp string) (res domain.UserAdmin, err error) {
	query := `SELECT id, user_id, no_ktp, nama_ktp, no_telp, alamat, kelurahan, kecamatan, kodepos, image_ktp, is_completed, updated_at, created_at FROM useradmin WHERE no_ktp = ?`

	list, err := m.fetch(ctx, query, no_ktp)
	if err != nil {
		return
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}
	return
}
func (m *postgreUserAdminRepo) Store(ctx context.Context, data *domain.UserAdmin) (err error) {
	query := ` INSERT useradmin SET user_id=?, no_ktp=?, nama_ktp=?, no_telp=?, alamat=?, kelurahan=?, kecamatan=?, kodepos=?, image_ktp=?, is_completed=?, updated_at=?, created_at=?`
	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, data.UserID.ID, data.NoKTP, data.NamaKTP, data.NoTelp, data.Alamat, data.Kelurahan, data.Kecamatan, data.Kodepos, data.ImageKTP, data.IsCompleted, data.UpdatedAt, data.CreatedAt)
	if err != nil {
		return
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return
	}
	data.ID = lastID
	return
}
func (m *postgreUserAdminRepo) Delete(ctx context.Context, id int64) (err error) {
	query := "DELETE FROM useradmin WHERE id = ?"

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return
	}

	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return
	}

	if rowsAfected != 1 {
		err = fmt.Errorf("weird  behavior. total affected: %d", rowsAfected)
		return
	}

	return
}
func (m *postgreUserAdminRepo) Update(ctx context.Context, dataUpdate *domain.UserAdmin) (err error) {
	query := `UPDATE useradmin SET user_id=?, no_ktp=?, nama_ktp=?, no_telp=?, alamat=?, kelurahan=?, kecamatan=?, kodepos=?, image_ktp=?, is_completed=?, updated_at=? WHERE id=?`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, dataUpdate.UserID.ID, dataUpdate.NoKTP, dataUpdate.NamaKTP, dataUpdate.NoTelp, dataUpdate.Alamat, dataUpdate.Kelurahan, dataUpdate.Kecamatan, dataUpdate.Kodepos, dataUpdate.ImageKTP, dataUpdate.IsCompleted, dataUpdate.UpdatedAt, dataUpdate.ID)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return
	}
	if affect != 1 {
		err = fmt.Errorf("weird  behavior. total affected: %d", affect)
		return
	}

	return
}
