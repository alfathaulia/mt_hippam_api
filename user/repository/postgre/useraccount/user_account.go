package postgre_useraccount

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/user/repository"
)

type postgreUserAccountRepo struct {
	DB *sql.DB
}

// NewPostgreUserAccountRepo will create an object that represent the domain.UserAccountRepository interface
func NewPostgreUserAccountRepo(DB *sql.DB) domain.UserAccountRepository {
	return &postgreUserAccountRepo{DB: DB}
}

func (m *postgreUserAccountRepo) fetch(ctx context.Context, query string, args ...interface{}) (result []domain.UserAccount, err error) {
	rows, err := m.DB.QueryContext(ctx, query, args...)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			logrus.Error(errRow)
		}
	}()

	result = make([]domain.UserAccount, 0)
	userID := int64(0)
	for rows.Next() {
		t := domain.UserAccount{}
		err = rows.Scan(
			&t.ID,
			&t.CustomerID,
			&userID,
			&t.Alamat,
			&t.PhotoProfile,
			&t.UpdatedAt,
			&t.CreatedAt,
		)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		t.UserID = domain.User{
			ID: userID,
		}
		result = append(result, t)
	}
	return result, nil
}

func (m *postgreUserAccountRepo) Fetch(ctx context.Context, cursor string, num int64) (res []domain.UserAccount, nextCursor string, err error) {
	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at FROM useraccount ORDER BY created_at LIMIT ?`

	decodeCursor, err := repository.DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", domain.ErrBadParamInput
	}
	res, err = m.fetch(ctx, query, decodeCursor, num)
	if err != nil {
		return nil, "", err
	}
	if len(res) == int(num) {
		nextCursor = repository.EncodeCursor(res[len(res)-1].CreatedAt)
	}
	return
}
func (m *postgreUserAccountRepo) GetByID(ctx context.Context, id int64) (res domain.UserAccount, err error) {
	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at
  						FROM useraccount WHERE ID = ?`

	list, err := m.fetch(ctx, query, id)
	if err != nil {
		return domain.UserAccount{}, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}

	return
}
func (m *postgreUserAccountRepo) GetByCustomerID(ctx context.Context, customer_id int64) (res domain.UserAccount, err error) {
	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at
  						FROM useraccount WHERE customer_id = ?`

	list, err := m.fetch(ctx, query, customer_id)
	if err != nil {
		return
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}
	return
}
func (m *postgreUserAccountRepo) Store(ctx context.Context, data *domain.UserAccount) (err error) {
	query := `INSERT  useraccount SET customer_id=? , user_id=? , alamat=?, photo_profile=?, updated_at=? , created_at=?`
	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, data.CustomerID, data.UserID.ID, data.Alamat, data.PhotoProfile, data.UpdatedAt, data.CreatedAt)
	if err != nil {
		return
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return
	}
	data.ID = lastID
	return
}
func (m *postgreUserAccountRepo) Delete(ctx context.Context, id int64) (err error) {
	query := "DELETE FROM useraccount WHERE id = ?"

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return
	}

	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return
	}

	if rowsAfected != 1 {
		err = fmt.Errorf("weird  behavior. total affected: %d", rowsAfected)
		return
	}

	return
}
func (m *postgreUserAccountRepo) Update(ctx context.Context, dataUpdate *domain.UserAccount) (err error) {
	query := `UPDATE  useraccount SET customer_id=? , user_id=? , alamat=?, photo_profile=?, updated_at=? WHERE id=?`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, dataUpdate.CustomerID, dataUpdate.UserID.ID, dataUpdate.Alamat, dataUpdate.PhotoProfile, dataUpdate.UpdatedAt, dataUpdate.ID)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return
	}
	if affect != 1 {
		err = fmt.Errorf("weird  behavior. total affected: %d", affect)
		return
	}

	return
}
