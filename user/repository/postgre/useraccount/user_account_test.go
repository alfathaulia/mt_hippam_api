package postgre_useraccount_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/user/repository"
	userAccountPostgreRepo "gitlab.com/alfathaulia/mt_hippam_api/user/repository/postgre/useraccount"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v2"
)

func TestFetch(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	mockUserAccount := []domain.UserAccount{
		{
			ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
		{
			ID: 2, CustomerID: 2, UserID: domain.User{ID: 2}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}
	rows := sqlmock.NewRows([]string{"id", "customer_id", "user_id", "alamat", "photo_profile", "updated_at", "created_at"}).
		AddRow(mockUserAccount[0].ID, mockUserAccount[0].CustomerID, mockUserAccount[0].UserID.ID, mockUserAccount[0].Alamat, mockUserAccount[0].PhotoProfile, mockUserAccount[0].UpdatedAt, mockUserAccount[0].CreatedAt).
		AddRow(mockUserAccount[1].ID, mockUserAccount[1].CustomerID, mockUserAccount[1].UserID.ID, mockUserAccount[1].Alamat, mockUserAccount[1].PhotoProfile, mockUserAccount[1].UpdatedAt, mockUserAccount[1].CreatedAt)

	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at FROM useraccount  ORDER BY created_at LIMIT ?`

	mock.ExpectQuery(query).WillReturnRows(rows)

	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)
	cursor := repository.EncodeCursor(mockUserAccount[1].CreatedAt)
	num := int64(2)
	list, nextCursor, err := a.Fetch(context.TODO(), cursor, num)
	assert.NoError(t, err)
	assert.NotEmpty(t, nextCursor)
	assert.Len(t, list, 2)
}

func TestGetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	mockUserAccount := []domain.UserAccount{
		{
			ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}

	rows := sqlmock.NewRows([]string{"id", "customer_id", "user_id", "alamat", "photo_profile", "updated_at", "created_at"}).
		AddRow(mockUserAccount[0].ID, mockUserAccount[0].CustomerID, mockUserAccount[0].UserID.ID, mockUserAccount[0].Alamat, mockUserAccount[0].PhotoProfile, mockUserAccount[0].UpdatedAt, mockUserAccount[0].CreatedAt)

	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at FROM useraccount WHERE ID = ?`

	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)

	num := int64(5)
	anArticle, err := a.GetByID(context.TODO(), num)
	assert.NoError(t, err)
	assert.NotNil(t, anArticle)
}

func TestGetByCustomerID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	mockUserAccount := []domain.UserAccount{
		{
			ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}
	rows := sqlmock.NewRows([]string{"id", "customer_id", "user_id", "alamat", "photo_profile", "updated_at", "created_at"}).AddRow(mockUserAccount[0].ID, mockUserAccount[0].CustomerID, mockUserAccount[0].UserID.ID, mockUserAccount[0].Alamat, mockUserAccount[0].PhotoProfile, mockUserAccount[0].UpdatedAt, mockUserAccount[0].CreatedAt)
	query := `SELECT id, customer_id, user_id, alamat, photo_profile, updated_at, created_at FROM useraccount WHERE customer_id = ?`
	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)
	CustomerID := int64(1)
	user, err := a.GetByCustomerID(context.TODO(), CustomerID)
	assert.NoError(t, err)
	assert.NotNil(t, user)
}

func TestStore(t *testing.T) {
	now := time.Now()
	user := &domain.UserAccount{
		ID:         1,
		CustomerID: 1,
		UserID: domain.User{
			ID:       1,
			Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now()}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: now, UpdatedAt: now,
	}
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "INSERT  useraccount SET customer_id=\\? , user_id=\\? , alamat=\\?, photo_profile=\\?, updated_at=\\? , created_at=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(user.CustomerID, user.UserID.ID, user.Alamat, user.PhotoProfile, user.UpdatedAt, user.CreatedAt).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)

	err = a.Store(context.TODO(), user)
	assert.NoError(t, err)
	assert.Equal(t, int64(12), user.ID)

}

func TestUpdate(t *testing.T) {
	now := time.Now()
	userAccount := &domain.UserAccount{
		ID:         1,
		CustomerID: 1,
		UserID: domain.User{
			ID:       1,
			Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now()}, Alamat: "sasap", PhotoProfile: "fasdfasdf", UpdatedAt: now, CreatedAt: now,
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "UPDATE useraccount SET customer_id=\\? , user_id=\\? , alamat=\\?, photo_profile=\\?, updated_at=\\?  WHERE id=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(userAccount.CustomerID, userAccount.UserID.ID, userAccount.Alamat, userAccount.PhotoProfile, userAccount.UpdatedAt, userAccount.ID).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)

	err = a.Update(context.TODO(), userAccount)
	assert.NoError(t, err)
}

func TestDelete(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "DELETE FROM useraccount WHERE id = \\?"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(12).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userAccountPostgreRepo.NewPostgreUserAccountRepo(db)

	num := int64(12)
	err = a.Delete(context.TODO(), num)
	assert.NoError(t, err)
}
