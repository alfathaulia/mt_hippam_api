package postgre_user_test

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/user/repository"
	userPostgreRepo "gitlab.com/alfathaulia/mt_hippam_api/user/repository/postgre/user"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v2"
)

var u = &domain.User{
	ID:             1,
	Username:       "user1",
	UnitCode:       "1222",
	HashedPassword: "hashPass",
	Role:           "user",
	UpdatedAt:      time.Now(),
	CreatedAt:      time.Now(),
}

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		logrus.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}
func TestFetch(t *testing.T) {
	db, mock := NewMock()

	mockUsers := []domain.User{
		{
			ID: u.ID, Username: u.Username, UnitCode: u.UnitCode, HashedPassword: u.HashedPassword, Role: u.Role, CreatedAt: u.CreatedAt, UpdatedAt: u.UpdatedAt,
		},
		{
			ID: 2, Username: "User 2", UnitCode: "122456", HashedPassword: "user2", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
		},
	}
	rows := sqlmock.NewRows([]string{"id", "username", "unit_code", "hashed_password", "role", "updated_at", "created_at"}).
		AddRow(mockUsers[0].ID, mockUsers[0].Username, mockUsers[0].UnitCode, mockUsers[0].HashedPassword, mockUsers[0].Role, mockUsers[0].UpdatedAt, mockUsers[0].CreatedAt).
		AddRow(mockUsers[1].ID, mockUsers[1].Username, mockUsers[1].UnitCode, mockUsers[1].HashedPassword, mockUsers[1].Role, mockUsers[1].UpdatedAt, mockUsers[1].CreatedAt)

	query := `SELECT id, username, unit_code, hashed_password, role, updated_at, created_at FROM user  ORDER BY created_at LIMIT ?`

	mock.ExpectQuery(query).WillReturnRows(rows)

	a := userPostgreRepo.NewPostgreUserRepo(db)
	cursor := repository.EncodeCursor(mockUsers[1].CreatedAt)
	num := int64(2)
	list, nextCursor, err := a.Fetch(context.TODO(), cursor, num)
	assert.NoError(t, err)
	assert.NotEmpty(t, nextCursor)
	assert.Len(t, list, 2)
	assert.Equal(t, u.Username, list[0].Username)
	assert.Equal(t, u.HashedPassword, list[0].HashedPassword)
	assert.Equal(t, u.UnitCode, list[0].UnitCode)
	assert.Equal(t, u.Role, list[0].Role)
	assert.Equal(t, u.CreatedAt, list[0].CreatedAt)
	assert.Equal(t, u.UpdatedAt, list[0].UpdatedAt)
	assert.NotZero(t, list[0].CreatedAt)

}

func TestGetByID(t *testing.T) {
	db, mock := NewMock()
	rows := sqlmock.NewRows([]string{"id", "username", "unit_code", "hashed_password", "role", "updated_at", "created_at"}).
		AddRow(1, u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.CreatedAt)

	query := `SELECT id, username, unit_code, hashed_password, role, updated_at, created_at FROM user WHERE ID = ?`

	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userPostgreRepo.NewPostgreUserRepo(db)

	num := int64(5)
	user, err := a.GetByID(context.TODO(), num)
	assert.NoError(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, u.Username, user.Username)
	assert.Equal(t, u.HashedPassword, user.HashedPassword)
	assert.Equal(t, u.UnitCode, user.UnitCode)
	assert.Equal(t, u.Role, user.Role)
	assert.Equal(t, u.CreatedAt, user.CreatedAt)
	assert.Equal(t, u.UpdatedAt, user.CreatedAt)
	assert.NotZero(t, user.CreatedAt)
}

func TestGetUserByUsername(t *testing.T) {
	db, mock := NewMock()

	rows := sqlmock.NewRows([]string{"id", "username", "unit_code", "hashed_password", "role", "updated_at", "created_at"}).AddRow(1, u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.CreatedAt)
	query := `SELECT id, username, unit_code, hashed_password, role, updated_at, created_at FROM user WHERE username = ?`
	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userPostgreRepo.NewPostgreUserRepo(db)
	username := "User 1"
	user, err := a.GetByUsername(context.TODO(), username)
	assert.NoError(t, err)
	assert.NotNil(t, user)
}

func TestStore(t *testing.T) {

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "INSERT  user SET username=\\? , unit_code=\\? , hashed_password=\\?, role=\\?, updated_at=\\? , created_at=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.CreatedAt).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userPostgreRepo.NewPostgreUserRepo(db)

	err = a.Store(context.TODO(), u)
	assert.NoError(t, err)
	assert.Equal(t, int64(12), u.ID)

}

func TestUpdate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	query := "UPDATE  user SET username=\\? , unit_code=\\? , hashed_password=\\?, role=\\?, updated_at=\\?  WHERE id=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.ID).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userPostgreRepo.NewPostgreUserRepo(db)

	err = a.Update(context.TODO(), u)
	assert.NoError(t, err)
}
func TestDelete(t *testing.T) {
	db, mock := NewMock()

	query := "DELETE FROM user WHERE id = \\?"

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(12).WillReturnResult(sqlmock.NewResult(12, 1))

	a := userPostgreRepo.NewPostgreUserRepo(db)

	num := int64(12)
	err := a.Delete(context.TODO(), num)
	assert.NoError(t, err)
}

func TestRegister(t *testing.T) {
	db, mock := NewMock()

	query := "INSERT  user SET username=\\? , unit_code=\\? , hashed_password=\\?, role=\\?, updated_at=\\? , created_at=\\?"
	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.CreatedAt).WillReturnResult(sqlmock.NewResult(12, 1))
	a := userPostgreRepo.NewPostgreUserRepo(db)

	err := a.Register(context.TODO(), u)
	assert.NoError(t, err)

}

func TestLogin(t *testing.T) {

	db, mock := NewMock()

	rows := sqlmock.NewRows([]string{"id", "username", "unit_code", "hashed_password", "role", "updated_at", "created_at"}).
		AddRow(1, u.Username, u.UnitCode, u.HashedPassword, u.Role, u.UpdatedAt, u.CreatedAt)

	query := `SELECT id, username, unit_code, hashed_password, role, updated_at, created_at FROM user WHERE username = ?`

	mock.ExpectQuery(query).WillReturnRows(rows)
	a := userPostgreRepo.NewPostgreUserRepo(db)
	username := u.Username
	unit_code := u.UnitCode
	pass := u.HashedPassword
	user, err := a.Login(context.TODO(), username, pass, unit_code)
	assert.NoError(t, err)
	assert.NotNil(t, user)
}
