package ucase_useradmin_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/domain/mocks"
	ucase "gitlab.com/alfathaulia/mt_hippam_api/user/usecase/useradmin"
)

func TestFetch(t *testing.T) {
	mockUserAdminRepo := new(mocks.UserAdminRepository)
	mockUserAdmin := domain.UserAdmin{
		ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "user1", NoTelp: "1", Alamat: "sasap", Kelurahan: "sasap", Kecamatan: "sooko", Kodepos: "61361", ImageKTP: "soo", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}
	mockListUserAdmin := make([]domain.UserAdmin, 0)
	mockListUserAdmin = append(mockListUserAdmin, mockUserAdmin)

	t.Run("success", func(t *testing.T) {
		mockUserAdminRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int64")).Return(mockListUserAdmin, "next-cursor", nil).Once()
		mockUser := domain.User{
			ID:             1,
			Username:       "user1",
			UnitCode:       "123",
			HashedPassword: "sdfasdf",
			Role:           "user",
			UpdatedAt:      time.Now(),
			CreatedAt:      time.Now(),
		}
		mockUserRepo := new(mocks.UserRepository)
		mockUserRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUser, nil)
		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)
		cursorExpected := "next-cursor"
		assert.Equal(t, cursorExpected, nextCursor)
		assert.NotEmpty(t, nextCursor)
		assert.NoError(t, err)
		assert.Len(t, list, len(mockListUserAdmin))
		mockUserAdminRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)

	})

	t.Run("error-failed", func(t *testing.T) {
		mockUserAdminRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int64")).Return(nil, "", errors.New("Unexpected Error")).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)

		assert.Empty(t, nextCursor)
		assert.Error(t, err)
		assert.Len(t, list, 0)
		mockUserAdminRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)

	})
}

func TestGetByID(t *testing.T) {
	mockUserAdminRepo := new(mocks.UserAdminRepository)
	mockUserAdmin := domain.UserAdmin{
		ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "user1", NoTelp: "1", Alamat: "sasap", Kelurahan: "sasap", Kecamatan: "sooko", Kodepos: "61361", ImageKTP: "soo", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}
	mockUser := domain.User{
		ID:             1,
		Username:       "user1",
		UnitCode:       "123",
		HashedPassword: "sdfasdf",
		Role:           "user",
		UpdatedAt:      time.Now(),
		CreatedAt:      time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAdminRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUserAdmin, nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		mockUserRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUser, nil)

		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		a, err := u.GetByID(context.TODO(), mockUserAdmin.ID)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockUserAdminRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockUserAdminRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAdmin{}, errors.New("Unexpected ")).Once()
		mockUserrepo := new(mocks.UserRepository)
		u := ucase.NewUserAdminUsecase(mockUserrepo, mockUserAdminRepo, time.Second*2)
		a, err := u.GetByID(context.TODO(), mockUserAdmin.ID)

		assert.Error(t, err)
		assert.Equal(t, domain.UserAdmin{}, a)

		mockUserAdminRepo.AssertExpectations(t)

	})
}

func TestStore(t *testing.T) {
	mockUserAdminRepo := new(mocks.UserAdminRepository)
	mockUserAdmin := domain.UserAdmin{
		ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "user1", NoTelp: "1", Alamat: "sasap", Kelurahan: "sasap", Kecamatan: "sooko", Kodepos: "61361", ImageKTP: "soo", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		tempMockUserAdmin := mockUserAdmin
		tempMockUserAdmin.ID = 0
		mockUserAdminRepo.On("GetByNoKTP", mock.Anything, mock.AnythingOfType("string")).Return(domain.UserAdmin{}, domain.ErrNotFound).Once()
		mockUserAdminRepo.On("Store", mock.Anything, mock.AnythingOfType("*domain.UserAdmin")).Return(nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		err := u.Store(context.TODO(), &tempMockUserAdmin)
		assert.NoError(t, err)

		assert.Equal(t, mockUserAdmin.NoKTP, tempMockUserAdmin.NoKTP)
		mockUserAdminRepo.AssertExpectations(t)
	})

	t.Run("existing-no_ktp", func(t *testing.T) {
		existingUserAdmin := mockUserAdmin
		mockUserAdminRepo.On("GetByNoKTP", mock.Anything, mock.AnythingOfType("string")).Return(existingUserAdmin, nil).Once()
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		err := u.Store(context.TODO(), &mockUserAdmin)

		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAdminRepo.AssertExpectations(t)

	})
}

func TestDelete(t *testing.T) {
	mockUserAdminRepo := new(mocks.UserAdminRepository)
	mockUserAdmin := domain.UserAdmin{
		ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "user1", NoTelp: "1", Alamat: "sasap", Kelurahan: "sasap", Kecamatan: "sooko", Kodepos: "61361", ImageKTP: "soo", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAdminRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUserAdmin, nil).Once()
		mockUserAdminRepo.On("Delete", mock.Anything, mock.AnythingOfType("int64")).Return(nil).Once()
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAdmin.ID)

		assert.NoError(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAdminRepo.AssertExpectations(t)
	})
	t.Run("useraccount-is-not-exist", func(t *testing.T) {
		mockUserAdminRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAdmin{}, nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAdmin.ID)
		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAdminRepo.AssertExpectations(t)
	})
	t.Run("error-happens-in-db", func(t *testing.T) {
		mockUserAdminRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAdmin{}, errors.New("Unexpected Error")).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAdmin.ID)

		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAdminRepo.AssertExpectations(t)
	})
}

func TestUpdate(t *testing.T) {
	mockUserAdminRepo := new(mocks.UserAdminRepository)
	mockUserAdmin := domain.UserAdmin{
		ID: 1, UserID: domain.User{ID: 1}, NoKTP: "1", NamaKTP: "user1", NoTelp: "1", Alamat: "sasap", Kelurahan: "sasap", Kecamatan: "sooko", Kodepos: "61361", ImageKTP: "soo", IsCompleted: false, CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAdminRepo.On("Update", mock.Anything, &mockUserAdmin).Once().Return(nil)
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAdminUsecase(mockUserRepo, mockUserAdminRepo, time.Second*2)

		err := u.Update(context.TODO(), &mockUserAdmin)
		assert.NoError(t, err)
		mockUserRepo.AssertExpectations(t)

	})
}
