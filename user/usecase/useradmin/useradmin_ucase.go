package ucase_useradmin

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"golang.org/x/sync/errgroup"
)

type userAdminUsecase struct {
	userRepo       domain.UserRepository
	userAdminRepo  domain.UserAdminRepository
	contextTimeout time.Duration
}

func NewUserAdminUsecase(u domain.UserRepository, uc domain.UserAdminRepository, timeout time.Duration) domain.UserAdminUsecase {
	return &userAdminUsecase{
		userRepo:       u,
		userAdminRepo:  uc,
		contextTimeout: timeout,
	}
}

func (a *userAdminUsecase) fillUserDetails(c context.Context, data []domain.UserAdmin) ([]domain.UserAdmin, error) {
	g, ctx := errgroup.WithContext(c)

	// Get the user's id
	mapUsers := map[int64]domain.User{}

	for _, user := range data {
		mapUsers[user.UserID.ID] = domain.User{}
	}
	// Using goroutine to fetch the user's detail
	chanUser := make(chan domain.User)
	for userID := range mapUsers {
		userID := userID
		g.Go(func() error {
			res, err := a.userRepo.GetByID(ctx, userID)
			if err != nil {
				return err
			}
			chanUser <- res
			return nil
		})
	}

	go func() {
		err := g.Wait()
		if err != nil {
			logrus.Error(err)
			return
		}
		close(chanUser)
	}()

	for user := range chanUser {
		if user != (domain.User{}) {
			mapUsers[user.ID] = user
		}
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	// merge the user's data
	for index, item := range data {
		if a, ok := mapUsers[item.UserID.ID]; ok {
			data[index].UserID = a
		}
	}
	return data, nil
}

func (m *userAdminUsecase) Fetch(ctx context.Context, cursor string, num int64) (res []domain.UserAdmin, nextCursor string, err error) {
	if num == 0 {
		num = 10
	}

	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()

	res, nextCursor, err = m.userAdminRepo.Fetch(ctx, cursor, num)
	if err != nil {
		return nil, "", err
	}

	res, err = m.fillUserDetails(ctx, res)
	if err != nil {
		nextCursor = ""
	}
	return

}

func (m *userAdminUsecase) GetByID(ctx context.Context, id int64) (res domain.UserAdmin, err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()

	res, err = m.userAdminRepo.GetByID(ctx, id)
	if err != nil {
		return domain.UserAdmin{}, err
	}

	resUser, err := m.userRepo.GetByID(ctx, res.UserID.ID)
	if err != nil {
		return domain.UserAdmin{}, err
	}
	res.UserID = resUser

	return
}

func (m *userAdminUsecase) GetByNoKTP(ctx context.Context, no_ktp string) (res domain.UserAdmin, err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	res, err = m.userAdminRepo.GetByNoKTP(ctx, no_ktp)
	if err != nil {
		return domain.UserAdmin{}, nil
	}
	resUser, err := m.userRepo.GetByID(ctx, res.UserID.ID)
	if err != nil {
		return domain.UserAdmin{}, err
	}
	res.UserID = resUser
	return
}

func (m *userAdminUsecase) Update(ctx context.Context, ar *domain.UserAdmin) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	ar.UpdatedAt = time.Now()
	return m.userAdminRepo.Update(ctx, ar)
}

func (m *userAdminUsecase) Store(ctx context.Context, a *domain.UserAdmin) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	existeUserAdmin, _ := m.userAdminRepo.GetByNoKTP(ctx, a.NoKTP)
	if existeUserAdmin != (domain.UserAdmin{}) {
		return domain.ErrConflict
	}
	err = m.userAdminRepo.Store(ctx, a)
	return
}

func (m *userAdminUsecase) Delete(ctx context.Context, id int64) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	existedUser, err := m.userAdminRepo.GetByID(ctx, id)
	if err != nil {
		return
	}
	if existedUser == (domain.UserAdmin{}) {
		return domain.ErrNotFound
	}
	return m.userAdminRepo.Delete(ctx, id)
}
