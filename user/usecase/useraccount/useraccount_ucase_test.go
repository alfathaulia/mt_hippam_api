package ucase_useraccount_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/domain/mocks"
	ucase "gitlab.com/alfathaulia/mt_hippam_api/user/usecase/useraccount"
)

func TestFetch(t *testing.T) {
	mockUserAccountRepo := new(mocks.UserAccountRepository)
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}
	mockListUserAccount := make([]domain.UserAccount, 0)
	mockListUserAccount = append(mockListUserAccount, mockUserAccount)

	t.Run("success", func(t *testing.T) {
		mockUserAccountRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int64")).Return(mockListUserAccount, "next-cursor", nil).Once()
		mockUser := domain.User{
			ID:             1,
			Username:       "user1",
			UnitCode:       "123",
			HashedPassword: "sdfasdf",
			Role:           "user",
			UpdatedAt:      time.Now(),
			CreatedAt:      time.Now(),
		}
		mockUserRepo := new(mocks.UserRepository)
		mockUserRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUser, nil)
		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)
		cursorExpected := "next-cursor"
		assert.Equal(t, cursorExpected, nextCursor)
		assert.NotEmpty(t, nextCursor)
		assert.NoError(t, err)
		assert.Len(t, list, len(mockListUserAccount))
		mockUserAccountRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)

	})

	t.Run("error-failed", func(t *testing.T) {
		mockUserAccountRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int64")).Return(nil, "", errors.New("Unexpected Error")).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)

		assert.Empty(t, nextCursor)
		assert.Error(t, err)
		assert.Len(t, list, 0)
		mockUserAccountRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)

	})
}

func TestGetByID(t *testing.T) {
	mockUserAccountRepo := new(mocks.UserAccountRepository)
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}
	mockUser := domain.User{
		ID:             1,
		Username:       "user1",
		UnitCode:       "123",
		HashedPassword: "sdfasdf",
		Role:           "user",
		UpdatedAt:      time.Now(),
		CreatedAt:      time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAccountRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUserAccount, nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		mockUserRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUser, nil)

		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		a, err := u.GetByID(context.TODO(), mockUserAccount.ID)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockUserAccountRepo.AssertExpectations(t)
		mockUserRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockUserAccountRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAccount{}, errors.New("Unexpected ")).Once()
		mockUserrepo := new(mocks.UserRepository)
		u := ucase.NewUserAccountUsecase(mockUserrepo, mockUserAccountRepo, time.Second*2)
		a, err := u.GetByID(context.TODO(), mockUserAccount.ID)

		assert.Error(t, err)
		assert.Equal(t, domain.UserAccount{}, a)

		mockUserAccountRepo.AssertExpectations(t)

	})
}

func TestStore(t *testing.T) {
	mockUserAccountRepo := new(mocks.UserAccountRepository)
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		tempMockUserAccount := mockUserAccount
		tempMockUserAccount.ID = 0
		mockUserAccountRepo.On("GetByCustomerID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAccount{}, domain.ErrNotFound).Once()
		mockUserAccountRepo.On("Store", mock.Anything, mock.AnythingOfType("*domain.UserAccount")).Return(nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		err := u.Store(context.TODO(), &tempMockUserAccount)
		assert.NoError(t, err)

		assert.Equal(t, mockUserAccount.CustomerID, tempMockUserAccount.CustomerID)
		mockUserAccountRepo.AssertExpectations(t)
	})

	t.Run("existing-customer_id", func(t *testing.T) {
		existingUserAccount := mockUserAccount
		mockUserAccountRepo.On("GetByCustomerID", mock.Anything, mock.AnythingOfType("int64")).Return(existingUserAccount, nil).Once()
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		err := u.Store(context.TODO(), &mockUserAccount)

		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAccountRepo.AssertExpectations(t)

	})
}

func TestDelete(t *testing.T) {
	mockUserAccountRepo := new(mocks.UserAccountRepository)
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAccountRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockUserAccount, nil).Once()
		mockUserAccountRepo.On("Delete", mock.Anything, mock.AnythingOfType("int64")).Return(nil).Once()
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAccount.ID)

		assert.NoError(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAccountRepo.AssertExpectations(t)
	})
	t.Run("useraccount-is-not-exist", func(t *testing.T) {
		mockUserAccountRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAccount{}, nil).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAccount.ID)
		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAccountRepo.AssertExpectations(t)
	})
	t.Run("error-happens-in-db", func(t *testing.T) {
		mockUserAccountRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(domain.UserAccount{}, errors.New("Unexpected Error")).Once()
		mockUserRepo := new(mocks.UserRepository)
		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)
		err := u.Delete(context.TODO(), mockUserAccount.ID)

		assert.Error(t, err)
		mockUserRepo.AssertExpectations(t)
		mockUserAccountRepo.AssertExpectations(t)
	})
}

func TestUpdate(t *testing.T) {
	mockUserAccountRepo := new(mocks.UserAccountRepository)
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: domain.User{ID: 1}, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		mockUserAccountRepo.On("Update", mock.Anything, &mockUserAccount).Once().Return(nil)
		mockUserRepo := new(mocks.UserRepository)

		u := ucase.NewUserAccountUsecase(mockUserRepo, mockUserAccountRepo, time.Second*2)

		err := u.Update(context.TODO(), &mockUserAccount)
		assert.NoError(t, err)
		mockUserRepo.AssertExpectations(t)

	})
}
