package ucase_useraccount

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"golang.org/x/sync/errgroup"
)

type userAccountUsecase struct {
	userRepo        domain.UserRepository
	userAccountRepo domain.UserAccountRepository
	contextTimeout  time.Duration
}

func NewUserAccountUsecase(u domain.UserRepository, uc domain.UserAccountRepository, timeout time.Duration) domain.UserAccountUsecase {
	return &userAccountUsecase{
		userRepo:        u,
		userAccountRepo: uc,
		contextTimeout:  timeout,
	}
}

func (a *userAccountUsecase) fillUserDetails(c context.Context, data []domain.UserAccount) ([]domain.UserAccount, error) {
	g, ctx := errgroup.WithContext(c)

	// Get the user's id
	mapUsers := map[int64]domain.User{}

	for _, user := range data {
		mapUsers[user.UserID.ID] = domain.User{}
	}
	// Using goroutine to fetch the user's detail
	chanUser := make(chan domain.User)
	for userID := range mapUsers {
		userID := userID
		g.Go(func() error {
			res, err := a.userRepo.GetByID(ctx, userID)
			if err != nil {
				return err
			}
			chanUser <- res
			return nil
		})
	}

	go func() {
		err := g.Wait()
		if err != nil {
			logrus.Error(err)
			return
		}
		close(chanUser)
	}()

	for user := range chanUser {
		if user != (domain.User{}) {
			mapUsers[user.ID] = user
		}
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	// merge the user's data
	for index, item := range data {
		if a, ok := mapUsers[item.UserID.ID]; ok {
			data[index].UserID = a
		}
	}
	return data, nil
}

func (m *userAccountUsecase) Fetch(ctx context.Context, cursor string, num int64) (res []domain.UserAccount, nextCursor string, err error) {
	if num == 0 {
		num = 10
	}

	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()

	res, nextCursor, err = m.userAccountRepo.Fetch(ctx, cursor, num)
	if err != nil {
		return nil, "", err
	}

	res, err = m.fillUserDetails(ctx, res)
	if err != nil {
		nextCursor = ""
	}
	return

}

func (m *userAccountUsecase) GetByID(ctx context.Context, id int64) (res domain.UserAccount, err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()

	res, err = m.userAccountRepo.GetByID(ctx, id)
	if err != nil {
		return domain.UserAccount{}, err
	}

	resUser, err := m.userRepo.GetByID(ctx, res.UserID.ID)
	if err != nil {
		return domain.UserAccount{}, err
	}
	res.UserID = resUser

	return
}

func (m *userAccountUsecase) GetByCustomerID(ctx context.Context, customer_id int64) (res domain.UserAccount, err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	res, err = m.userAccountRepo.GetByCustomerID(ctx, customer_id)
	if err != nil {
		return domain.UserAccount{}, nil
	}
	resUser, err := m.userRepo.GetByID(ctx, res.UserID.ID)
	if err != nil {
		return domain.UserAccount{}, err
	}
	res.UserID = resUser
	return
}

func (m *userAccountUsecase) Update(ctx context.Context, ar *domain.UserAccount) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	ar.UpdatedAt = time.Now()
	return m.userAccountRepo.Update(ctx, ar)
}

func (m *userAccountUsecase) Store(ctx context.Context, a *domain.UserAccount) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	existeUserAccount, _ := m.userAccountRepo.GetByCustomerID(ctx, a.CustomerID)
	if existeUserAccount != (domain.UserAccount{}) {
		return domain.ErrConflict
	}
	err = m.userAccountRepo.Store(ctx, a)
	return
}

func (m *userAccountUsecase) Delete(ctx context.Context, id int64) (err error) {
	ctx, cancel := context.WithTimeout(ctx, m.contextTimeout)
	defer cancel()
	existedUser, err := m.userAccountRepo.GetByID(ctx, id)
	if err != nil {
		return
	}
	if existedUser == (domain.UserAccount{}) {
		return domain.ErrNotFound
	}
	return m.userAccountRepo.Delete(ctx, id)
}
