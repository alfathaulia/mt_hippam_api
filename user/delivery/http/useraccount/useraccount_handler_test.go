package http_useraccount_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/bxcodec/faker"
	"github.com/labstack/echo/v4"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/domain/mocks"
	userHttp "gitlab.com/alfathaulia/mt_hippam_api/user/delivery/http/useraccount"
)

func TestFetch(t *testing.T) {
	var mockUserAccount domain.UserAccount
	err := faker.FakeData(&mockUserAccount)
	assert.NoError(t, err)
	mockUCase := new(mocks.UserAccountUsecase)
	mockListUserAccount := make([]domain.UserAccount, 0)
	mockListUserAccount = append(mockListUserAccount, mockUserAccount)

	num := 1
	cursor := "2"
	mockUCase.On("Fetch", mock.Anything, cursor, int64(num)).Return(mockListUserAccount, "10", nil)
	e := echo.New()
	req, err := http.NewRequest(echo.GET, "/users/account?num=1&cursor="+cursor, strings.NewReader(""))
	assert.NoError(t, err)

	w := httptest.NewRecorder()
	c := e.NewContext(req, w)

	handler := userHttp.UserAccountHandler{
		UCUsecase: mockUCase,
	}

	err = handler.FetchUserAccount(c)
	require.NoError(t, err)
	responseCursor := w.Header().Get("X-Cursor")
	assert.Equal(t, "10", responseCursor)
	assert.Equal(t, http.StatusOK, w.Code)
	mockUCase.AssertExpectations(t)
}

func TestGetByID(t *testing.T) {
	var mockUserAccount domain.UserAccount
	err := faker.FakeData(&mockUserAccount)
	assert.NoError(t, err)

	mockUCase := new(mocks.UserAccountUsecase)

	num := int(mockUserAccount.ID)

	mockUCase.On("GetByID", mock.Anything, int64(num)).Return(mockUserAccount, nil)

	e := echo.New()
	req, err := http.NewRequest(echo.GET, "/users/account"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("users/account/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))
	handler := userHttp.UserAccountHandler{
		UCUsecase: mockUCase,
	}
	err = handler.GetByID(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusOK, rec.Code)
	mockUCase.AssertExpectations(t)
}

//TODO Test Fail

func TestStore(t *testing.T) {
	mockUser := domain.User{
		ID:             1,
		Username:       "user1",
		UnitCode:       "1222",
		HashedPassword: "hashPass",
		Role:           "user",
		UpdatedAt:      time.Now(),
		CreatedAt:      time.Now(),
	}
	mockUserAccount := domain.UserAccount{
		ID: 1, CustomerID: 1, UserID: mockUser, Alamat: "sasap", PhotoProfile: "fasdfasdf", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUserAccount := mockUserAccount
	tempMockUserAccount.ID = int64(1)
	mockUCase := new(mocks.UserAccountUsecase)

	j, err := json.Marshal(tempMockUserAccount)
	assert.NoError(t, err)

	mockUCase.On("Store", mock.Anything, mock.AnythingOfType("*domain.UserAccount")).Return(nil).Once()

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/users/account", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/users/account")

	handler := userHttp.UserAccountHandler{
		UCUsecase: mockUCase,
	}
	err = handler.Store(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestDelete(t *testing.T) {
	var mockUserAccount domain.UserAccount
	err := faker.FakeData(&mockUserAccount)
	assert.NoError(t, err)

	mockUCase := new(mocks.UserAccountUsecase)

	num := int(mockUserAccount.ID)

	mockUCase.On("Delete", mock.Anything, int64(num)).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.DELETE, "/user/account/"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("user/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))
	handler := userHttp.UserAccountHandler{
		UCUsecase: mockUCase,
	}
	err = handler.Delete(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusNoContent, rec.Code)
	mockUCase.AssertExpectations(t)
}
