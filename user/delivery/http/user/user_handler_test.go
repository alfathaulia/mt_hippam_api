package http_user_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/bxcodec/faker"
	"github.com/labstack/echo/v4"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/alfathaulia/mt_hippam_api/domain"
	"gitlab.com/alfathaulia/mt_hippam_api/domain/mocks"
	userHttp "gitlab.com/alfathaulia/mt_hippam_api/user/delivery/http/user"
)

func TestFetch(t *testing.T) {
	var mockUser domain.User
	err := faker.FakeData(&mockUser)
	assert.NoError(t, err)
	mockUCase := new(mocks.UserUsecase)
	mockListUser := make([]domain.User, 0)
	mockListUser = append(mockListUser, mockUser)

	num := 1
	cursor := "2"
	mockUCase.On("Fetch", mock.Anything, cursor, int64(num)).Return(mockListUser, "10", nil)
	e := echo.New()
	req, err := http.NewRequest(echo.GET, "/user?num=1&cursor="+cursor, strings.NewReader(""))
	assert.NoError(t, err)

	w := httptest.NewRecorder()
	c := e.NewContext(req, w)

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}

	err = handler.FetchUser(c)
	require.NoError(t, err)
	responseCursor := w.Header().Get("X-Cursor")
	assert.Equal(t, "10", responseCursor)
	assert.Equal(t, http.StatusOK, w.Code)
	mockUCase.AssertExpectations(t)
}

func TestGetByID(t *testing.T) {
	var mockUser domain.User
	err := faker.FakeData(&mockUser)
	assert.NoError(t, err)

	mockUCase := new(mocks.UserUsecase)

	num := int(mockUser.ID)

	mockUCase.On("GetByID", mock.Anything, int64(num)).Return(mockUser, nil)

	e := echo.New()
	req, err := http.NewRequest(echo.GET, "/user/"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("user/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))
	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.GetByID(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusOK, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestStore(t *testing.T) {
	mockUser := domain.User{
		Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUser := mockUser
	tempMockUser.ID = 0
	mockUCase := new(mocks.UserUsecase)

	j, err := json.Marshal(tempMockUser)
	assert.NoError(t, err)

	mockUCase.On("Store", mock.Anything, mock.AnythingOfType("*domain.User")).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/user", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/user")

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.Store(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestDelete(t *testing.T) {
	var mockUser domain.User
	err := faker.FakeData(&mockUser)
	assert.NoError(t, err)

	mockUCase := new(mocks.UserUsecase)

	num := int(mockUser.ID)

	mockUCase.On("Delete", mock.Anything, int64(num)).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.DELETE, "/user/"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("user/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))
	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.Delete(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusNoContent, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestRegister(t *testing.T) {
	mockUser := domain.User{
		Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUser := mockUser
	tempMockUser.ID = 0
	mockUCase := new(mocks.UserUsecase)

	j, err := json.Marshal(tempMockUser)
	assert.NoError(t, err)

	mockUCase.On("Register", mock.Anything, mock.AnythingOfType("*domain.User")).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/users/register", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/users/register")

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.Register(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestLogin(t *testing.T) {
	mockUser := domain.User{

		ID: 1, Username: "User1", UnitCode: "122456", HashedPassword: "user1", Role: "user", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUser := mockUser
	tempMockUser.ID = 1
	mockUCase := new(mocks.UserUsecase)

	j, err := json.Marshal(tempMockUser)
	assert.NoError(t, err)

	mockUCase.On("Login", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(mockUser, nil).Once()

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/users/login", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.SetBasicAuth(mockUser.Username, mockUser.HashedPassword)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/users/login")

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.Login(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestCreateAdmin(t *testing.T) {
	mockUser := domain.User{
		Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "admin", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUser := mockUser
	tempMockUser.ID = 0
	mockUCase := new(mocks.UserUsecase)

	j, err := json.Marshal(tempMockUser)
	assert.NoError(t, err)

	mockUCase.On("CreateAdmin", mock.Anything, mock.AnythingOfType("*domain.User")).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/user/createadmin", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/user/createadmin")

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.CreateAdmin(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestCreateStaff(t *testing.T) {
	mockUser := domain.User{
		Username: "User 1", UnitCode: "122456", HashedPassword: "user1", Role: "staff", CreatedAt: time.Now(), UpdatedAt: time.Now(),
	}

	tempMockUser := mockUser
	tempMockUser.ID = 0
	mockUCase := new(mocks.UserUsecase)

	j, err := json.Marshal(tempMockUser)
	assert.NoError(t, err)

	mockUCase.On("CreateStaff", mock.Anything, mock.AnythingOfType("*domain.User")).Return(nil)

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/user/createadmin", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/user/createadmin")

	handler := userHttp.UserHandler{
		UUsecase: mockUCase,
	}
	err = handler.CreateStaff(c)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, rec.Code)
	mockUCase.AssertExpectations(t)
}
